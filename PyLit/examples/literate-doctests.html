


<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19b2.dev: https://docutils.sourceforge.io/" />

    <title>How to write literate doctests with PyLit &#8212; PyLit</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/pylit-sphinx.css" type="text/css" />
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <link rel="shortcut icon" href="../_static/favicon.ico"/>
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="testfile_literate" href="testfile_literate.py.html" />
    <link rel="prev" title="simplestates.py" href="simplestates.py.html" />
   
  <link rel="stylesheet" href="../_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="how-to-write-literate-doctests-with-pylit">
<h1>How to write literate doctests with PyLit<a class="headerlink" href="#how-to-write-literate-doctests-with-pylit" title="Permalink to this headline">¶</a></h1>
<section id="python-doctest-module">
<h2>Python Doctest Module<a class="headerlink" href="#python-doctest-module" title="Permalink to this headline">¶</a></h2>
<p><em>Doctests</em> are a literate way of testing a Python script.  They are
supported by the <a class="reference external" href="http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#doctest-blocks">doctest block</a> syntax in reStructuredText.</p>
<blockquote>
<div><p>The <a class="reference external" href="http://docs.python.org/library/doctest.html">doctest module</a> searches strings for pieces of text that look like
interactive Python sessions, and then executes those sessions to verify
that they work exactly as shown.</p>
<p>There are several common ways to use doctest:</p>
<ul class="simple">
<li><p>To check that a module’s docstrings are up-to-date by verifying that all
interactive examples still work as documented.</p></li>
<li><p>To perform regression testing by verifying that interactive examples
from a test file or a test object work as expected.</p></li>
<li><p>To write tutorial documentation for a package, liberally illustrated
with input-output examples. Depending on whether the examples or the
expository text are emphasised, this has the flavor of “literate
testing” or “executable documentation”.</p></li>
</ul>
<p class="attribution">—Python Library Reference for the <a class="reference external" href="http://docs.python.org/library/doctest.html">doctest module</a></p>
</div></blockquote>
</section>
<section id="doctest-and-pylit">
<h2>Doctest and PyLit<a class="headerlink" href="#doctest-and-pylit" title="Permalink to this headline">¶</a></h2>
<p>The most common way to use the doctest module is to check examples in all
<em>docstrings</em> of a module with <code class="docutils literal notranslate"><span class="pre">doctest.testmod()</span></code>, e.g.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="k">if</span> <span class="vm">__name__</span> <span class="o">==</span> <span class="s2">&quot;__main__&quot;</span><span class="p">:</span>
    <span class="kn">import</span> <span class="nn">doctest</span>
    <span class="n">doctest</span><span class="o">.</span><span class="n">testmod</span><span class="p">()</span>
</pre></div>
</div>
<p>You can also check a <em>text</em> file as if it were a docstring by calling
doctest from the command line, e.g.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">sh</span><span class="o">&gt;</span> <span class="n">python</span> <span class="o">-</span><span class="n">c</span> <span class="s2">&quot;import doctest; doctest.testfile(&#39;example.py.txt&#39;)&quot;</span>
</pre></div>
</div>
<p>However, both methods will not check doctest blocks in comments. This is
why they will fail to find doctests in the text blocks of a literate source
in code format. (See the <a class="reference external" href="../tutorial/index.html#doctests">tutorial</a> for discussion.)</p>
<p>You can of course convert your source to text form and run
<cite>doctest.testfile</cite> on it. To simplify the task, Pylit supports
<a class="reference external" href="http://docs.python.org/library/doctest.html">Python doctests</a> in a literate source with an option:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">sh</span><span class="o">&gt;</span> <span class="n">pylit</span> <span class="o">--</span><span class="n">doctest</span> <span class="n">example</span><span class="o">.</span><span class="n">py</span>
</pre></div>
</div>
<p>will check a literate source file for all doctests regardless of their
location in docstrings or text parts. It can work with both, text or code
format.</p>
<p>In order to do this, it will read the file, transform a code source to text
format on-the-fly and feed the result to a <a class="reference external" href="http://docs.python.org/library/doctest.html#doctestparser-objects">DocTestParser</a> object.
I.e., no text source file will be created if <code class="docutils literal notranslate"><span class="pre">pylit</span> <span class="pre">--doctest</span></code> is
called on a code source file.</p>
<p>This way, it is possible to separate basic examples in doc strings from
additional test in the literate source.</p>
</section>
<section id="examples">
<h2>Examples<a class="headerlink" href="#examples" title="Permalink to this headline">¶</a></h2>
<dl>
<dt><a class="reference external" href="testmod_literate.py.html">testmod_literate</a></dt><dd><p>is a “literate version” of the example in the <a class="reference external" href="http://docs.python.org/library/doctest.html">doctest module</a> doc that
does a self test when called as <cite>__main__</cite>.</p>
<p>It calls <cite>pylit.run_doctest</cite> to find tests in both docstrings and
documentation blocks.</p>
<p>Test this file with:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="c1">#&gt; python testmod_literate.py</span>
<span class="mi">0</span> <span class="n">failures</span> <span class="ow">in</span> <span class="mi">14</span> <span class="n">tests</span>
</pre></div>
</div>
<p>Sources: <a class="reference external" href="testmod_literate.py">testmod_literate.py</a>, <a class="reference external" href="testmod_literate.py.txt">testmod_literate.py.txt</a></p>
</dd>
<dt><a class="reference external" href="testfile_literate.py.html">testfile_literate</a></dt><dd><p>is a “literate version” of the example in the <a class="reference external" href="http://docs.python.org/library/doctest.html">doctest module</a> doc
adapted for being tested with <code class="docutils literal notranslate"><span class="pre">pylit</span> <span class="pre">--doctest</span></code>.</p>
<p>Test this file with:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span>  #&gt; pylit --doctest testfile_literate.py
  0 failures in 19 tests

It imports itself in the usage example and has a non-testing (albeit
silly) default action if called from the command line.

Sources: `&lt;testfile_literate.py&gt;`_, `&lt;testfile_literate.py.txt&gt;`_
</pre></div>
</div>
</dd>
</dl>
</section>
</section>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="../index.html">
              <img class="logo" src="../_static/pylit-bold-framed.png" alt="Logo"/>
            </a></p>
<h1 class="logo"><a href="../index.html">PyLit</a></h1>








<h3>Navigation</h3>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../features.html">Features</a></li>
</ul>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../usage.html">Usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="../tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="index.html">Examples</a></li>
<li class="toctree-l1"><a class="reference internal" href="../literate-programming.html">Literate Programming</a></li>
</ul>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../download/index.html">Requirements</a></li>
<li class="toctree-l1"><a class="reference internal" href="../download/index.html#download">Download</a></li>
<li class="toctree-l1"><a class="reference internal" href="../download/index.html#installation">Installation</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../index.html">Documentation overview</a><ul>
  <li><a href="index.html">Examples</a><ul>
      <li>Previous: <a href="simplestates.py.html" title="previous chapter">simplestates.py</a></li>
      <li>Next: <a href="testfile_literate.py.html" title="next chapter">testfile_literate</a></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
 <p class="thanks">
  <!-- Thanks to	      -->
  <!-- for hosting this site. -->
 </p>

  </body>
</html>