


<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19b2.dev: https://docutils.sourceforge.io/" />

    <title>Literate Programming &#8212; PyLit</title>
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="_static/pylit-sphinx.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="shortcut icon" href="_static/favicon.ico"/>
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Requirements" href="download/index.html" />
    <link rel="prev" title="conf.py – Sphinx configuration for PyLit" href="examples/conf.py.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="literate-programming">
<h1>Literate Programming<a class="headerlink" href="#literate-programming" title="Permalink to this headline">¶</a></h1>
<section id="the-philosophy-of-literate-programming">
<h2>The Philosophy of Literate Programming<a class="headerlink" href="#the-philosophy-of-literate-programming" title="Permalink to this headline">¶</a></h2>
<p>The philosophy of <em>literate programming</em> (LP) and the term itself were the
creation of Donald Knuth, the author of <cite>The Art of
Computer Programming</cite>, the TeX typesetting system, and other works of the
programming art.</p>
<blockquote>
<div><p>I believe that the time is ripe for significantly better documentation of
programs, and that we can best achieve this by considering programs to be
works of literature. Hence, my title: “Literate Programming.”</p>
<p>Let us change our traditional attitude to the construction of programs:
Instead of imagining that our main task is to instruct a computer what to
do, let us concentrate rather on explaining to humans what we want the
computer to do.</p>
<p class="attribution">—Donald E. Knuth, <a class="reference external" href="http://www.literateprogramming.com/knuthweb.pdf">Literate Programming</a>,  1984</p>
</div></blockquote>
<p id="definition">The <a class="reference external" href="http://www.literateprogramming.com/lpfaq.pdf">Literate Programming FAQ</a> gives the following definition:</p>
<blockquote>
<div><p><em>Literate programming</em> is the combination of documentation and source
together in a fashion suited for reading by human beings.</p>
</div></blockquote>
<p>and the <a class="reference external" href="http://en.wikipedia.org/wiki/Literate_programming">Wikipedia</a> says:</p>
<blockquote>
<div><p><em>Literate programming</em> is a philosophy of computer programming based on
the premise that a computer program should be written with human
readability as a primary goal, similarly to a work of literature.</p>
<p>According to this philosophy, programmers should aim for a “literate”
style in their programming just as writers aim for an intelligible and
articulate style in their writing. This philosophy contrasts with the
mainstream view that the programmer’s primary or sole objective is to
create source code and that documentation should only be a secondary
objective.</p>
</div></blockquote>
<p>John Max Skaller put the philosophy of literate programming in one sentence</p>
<blockquote>
<div><p>The idea is that you do not document programs (after the fact), but
write documents that <em>contain</em> the programs.</p>
<p class="attribution">—John Max Skaller in a <a class="reference external" href="http://www.ibm.com/developerworks/library/l-pyth7.html">Charming Python interview</a>.</p>
</div></blockquote>
</section>
<section id="lp-technique-and-systems">
<h2>LP Technique and Systems<a class="headerlink" href="#lp-technique-and-systems" title="Permalink to this headline">¶</a></h2>
<p>Donald Knuth not only conceived the <em>philosophy</em> of literate programming,
he also wrote the first LP <em>system</em> <a class="reference external" href="http://www.literateprogramming.com/knuthweb.pdf">WEB</a> that
implements the <em>technique</em> of literate programming:</p>
<blockquote id="technique-of-literate-programming">
<div><p>WEB itself is chiefly a combination of two other languages:</p>
<ol class="arabic simple">
<li><p>a document formatting language and</p></li>
<li><p>a programming language.</p></li>
</ol>
<p>My prototype WEB system uses TEX as the document formatting language
and PASCAL as the programming language, but the same principles would
apply equally well if other languages were substituted. […]. The main
point is that WEB is inherently bilingual, and that such a combination
of languages proves to be much more powerful than either single
language by itself. WEB does not make the other languages obsolete; on
the contrary, it enhances them.</p>
<p class="attribution">—Donald E. Knuth, <a class="reference external" href="http://www.literateprogramming.com/knuthweb.pdf">Literate Programming</a>,  1984</p>
</div></blockquote>
<p id="lp-system"><span id="literate-programming-system"></span>There exists a famous <em>statement of requirements</em> for a LP
system:</p>
<blockquote id="statement-of-requirements">
<div><p><em>Literate programming systems</em> have the following properties:</p>
<ol class="arabic simple">
<li><p>Code and extended, detailed comments are intermingled.</p></li>
<li><p>The code sections can be written in whatever order is best for people
to understand, and are re-ordered automatically when the computer needs
to run the program.</p></li>
<li><p>The program and its documentation can be handsomely typeset into a
single article that explains the program and how it works. Indices and
cross-references are generated automatically.</p></li>
</ol>
<p class="attribution">—Mark-Jason Dominus <a class="reference external" href="http://www.perl.com/pub/a/tchrist/litprog.html">POD is not Literate Programming</a></p>
</div></blockquote>
<p>The article <a class="reference internal" href="#contemporary-literate-programming" id="citation-reference-1"><span>[contemporary-literate-programming]</span></a> provides a survey on
different implementations of the literate programming technique.</p>
</section>
<section id="lp-with-restructuredtext">
<h2>LP with reStructuredText<a class="headerlink" href="#lp-with-restructuredtext" title="Permalink to this headline">¶</a></h2>
<p>PyLit fails the <a class="reference internal" href="#statement-of-requirements">statement of requirements</a> for a LP
system, as it does not provide for code re-ordering, automatic indices and
cross-references.</p>
<p>According to the Wikipedia, PyLit qualifies as <em>semi-literate</em> (while POD is
classified as <a class="reference external" href="https://en.wikipedia.org/wiki/Literate_programming#Contrast_with_documentation_generation">embedded documentation</a>):</p>
<blockquote>
<div><p>It is the fact that documentation can be written freely whereas code must
be marked in a special way that makes the difference between semi-literate
programming and excessive documenting, where the documentation is embedded
into the code as comments.</p>
<p class="attribution">—<a class="reference external" href="http://en.wikipedia.org/wiki/Literate_programming">Wikipedia entry on literate programming</a></p>
</div></blockquote>
<p>However, it is argued that PyLit is an <em>educated choice</em> as tool for the
<a class="reference internal" href="#technique-of-literate-programming">technique of literate programming</a>:</p>
<ul class="simple">
<li><p>PyLit is inherently bilingual. The document containing the program is
a combination of:</p>
<ol class="arabic simple">
<li><p>a document formatting language (<a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a>) and</p></li>
<li><p>a programming language.</p></li>
</ol>
</li>
<li><p>Program and documentation can be typeset in a single article as well as
converted to a hyperlinked HTML document representing the program’s
“web-like” structure. For indexing and cross-references, PyLit relies
on the rich features of the <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> document formatting
language. Auto-generating of cross references and indices is
possible if the reStructuredText sources are converted with <a class="reference external" href="https://www.sphinx-doc.org/">Sphinx</a></p></li>
</ul>
<p>Dispensing with code re-ordering enables the <a class="reference external" href="/features/index.html#dual-source">dual-source</a> concept. Code
re-ordering is considered less important when LP is not
seen as an <em>alternative</em> to structured programming but a technique
<em>orthogonal</em> to structured, object oriented or functional programming
styles:</p>
<blockquote>
<div><p>The computer science and programming pioneers, like Wirth and Knuth,
used another programming style than we recommend today. One notable
difference is the use of procedure abstractions. When Wirth and Knuth
wrote their influential books and programming, procedure calls were used
with care, partly because they were relatively expensive. Consequently,
they needed an extra level of structuring within a program or procedure.
‘Programming by stepwise refinement’ as well as ‘literate programming’
can be seen as techniques and representations to remedy the problem. The
use of many small (procedure) abstractions goes in another direction. If
you have attempted to use ‘literate programming’ on a program (maybe an
object-oriented program) with lots of small abstractions, you will
probably have realized that there is a misfit between, on the one hand,
being forced to name literal program fragments (scraps) and on the
other, named program abstractions. We do not want to leave the
impression that this is a major conceptual problem, but it is the
experience of the author that we need a variation of literate
programming, which is well-suited to programs with many, small (named)
abstractions.</p>
<p class="attribution">—Kurt Nørmark: <a class="reference external" href="http://www.cs.aau.dk/~normark/litpro/issues-and-problems.html">Literate Programming - Issues and Problems</a></p>
</div></blockquote>
<p>Ordering code for optimal presentation to humans is still a problem in
many “traditional” programming languages. Pythons dynamic name resolution
allows a great deal of flexibility in the order of code.</p>
</section>
<section id="existing-frameworks">
<h2>Existing Frameworks<a class="headerlink" href="#existing-frameworks" title="Permalink to this headline">¶</a></h2>
<p>There are many LP systems available, both sophisticated
and lightweight. This non-comprehensive list compares them to PyLit.</p>
<section id="web-and-friends">
<h3>WEB and friends<a class="headerlink" href="#web-and-friends" title="Permalink to this headline">¶</a></h3>
<p>The first published literate programming system was <a class="reference external" href="http://www.literateprogramming.com/knuthweb.pdf">WEB</a> by Donald Knuth. It
uses Pascal as programming language and TeX as document formatting language.</p>
<p><a class="reference external" href="https://tug.ctan.org/web/cweb/cwebman.pdf">CWEB</a> is a newer version of WEB for the C programming language. Other
implementations of the concept are the “multi-lingual” <a class="reference external" href="https://www.cs.tufts.edu/~nr/noweb/">noweb</a> and
<a class="reference external" href="http://www.ross.net/funnelweb/">FunnelWeb</a>. All of them use TeX as document formatting language.</p>
<p>Some of the problems with WEB-like systems are:</p>
<ul>
<li><p>The steep learning curve for mastering TeX, special WEB commands and
the actual programming language in parallel discourages potential literate
programmers.</p></li>
<li><p>While a printout has the professional look of a TeX-typeset essay, the
programmer will spend most time viewing and editing the source. A WEB
source in a text editor is not “suited for reading by human beings”:</p>
<blockquote>
<div><p>In Knuth’s work, beautiful literate programs were woven from ugly mixes of
markups in combined program and documentation files. The literate program
was directly targeted towards paper and books. Of obvious reasons,
programmers do not very often print their programs on paper. (And as
argued above, few of us intend to publish our programs in a book). Within
hours, such a program print out is no longer up-to-date. And furthermore,
being based on a paper representation, we cannot use the computers dynamic
potentials, such as outlining and searching.</p>
<p class="attribution">—Kurt Nørmark: <a class="reference external" href="http://www.cs.aau.dk/~normark/litpro/issues-and-problems.html">Literate Programming - Issues and Problems</a></p>
</div></blockquote>
</li>
</ul>
<p>How does PyLit differ from the web-like LP frameworks
(For a full discussion of PyLit’s concepts see the <a class="reference external" href="/features/index.html">Features</a> page.):</p>
<ul>
<li><p>PyLit is not a complete <a class="reference internal" href="#lp-system">LP system</a> but a simple tool.
It does not support re-arranging of named code chunks.</p></li>
<li><p>PyLit uses <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> as document formatting language.
The literate program source is legible in any text editor.</p>
<p>Even the code source is “in a fashion suited for reading by human beings”,
in opposition to source code produced by a full grown web system:</p>
<blockquote>
<div><p>Just consider the output of <code class="docutils literal notranslate"><span class="pre">tangle</span></code> to be object code: You can run
it, but you don’t want to look at it.</p>
<p class="attribution">—<a class="reference external" href="http://www.perl.com/pub/a/tchrist/litprog.html">http://www.perl.com/pub/a/tchrist/litprog.html</a></p>
</div></blockquote>
</li>
<li><p>PyLit is a <em>bidirectional</em> text &lt;-&gt; code converter.</p>
<ul class="simple">
<li><p>You can start a literate program from a “normal” code source by
converting to a text source and adding the documentation parts.</p></li>
<li><p>You can edit code in its “native” mode in any decent text editor or IDE.</p></li>
<li><p>You can debug and fix code with the standard tools using the code source
Line numbers are the same in text and code format. Once ready, forward
your changes to the text version with a code-&gt;text conversion.</p></li>
</ul>
<ul class="simple">
<li><p>PyLit cannot support export to several code files from one text source.</p></li>
</ul>
</li>
</ul>
</section>
<section id="interscript">
<h3>Interscript<a class="headerlink" href="#interscript" title="Permalink to this headline">¶</a></h3>
<p><a class="reference external" href="http://interscript.sourceforge.net/">Interscript</a> is a complex framework that uses Python both for its
implementation and to supply scripting services to client sourceware.</p>
<blockquote>
<div><p>Interscript is different, because the fundamental paradigm is extended so
that there are three kinds of LP sections in file:</p>
<ol class="arabic simple">
<li><p>Code sections</p></li>
<li><p>Documentation sections</p></li>
<li><p>Scripting sections</p></li>
</ol>
<p>where the scripting sections […] control and possibly generate both
documentation and code.</p>
<p class="attribution">—<a class="reference external" href="http://interscript.sourceforge.net/">Interscript</a> homepage</p>
</div></blockquote>
</section>
<section id="ly">
<h3>Ly<a class="headerlink" href="#ly" title="Permalink to this headline">¶</a></h3>
<p><a class="reference external" href="http://lyterate.sourceforge.net/intro.html">Ly</a>, the “lyterate programming thingy.” is an engine for Literate Programming.
The design considerations look very much like the ones for PyLit:</p>
<blockquote>
<div><p>So why would anybody use Ly instead of the established Literate
Programming tools like WEB or noweb?</p>
<blockquote>
<div><ul class="simple">
<li><p>Elegance. I do not consider the source code of WEB or noweb to be
particularly nice. I especially wanted a syntax that allows me to easily
write a short paragraph, then two or three lines of code, then another
short paragraph. Ly accomplishes this by distinguishing code from text
through indentation.</p></li>
<li><p>HTML. WEB and CWEB are targeted at outputting TeX. This is a valid
choice, but not mine, as I want to create documents with WWW links, and
as I want to re-create documents each time I change something– both of
these features don’t go well together with a format suited to printout,
like TeX is. (Of course you can create HTML from LaTeX, but if my output
format is HTML, I want to be able to write HTML directly.)</p></li>
<li><p>Python. I originally created Ly for a Python-based project, and
therefore I wanted it to run everywhere where Python runs. WEB and noweb
are mostly targeted at UNIX-like platforms.</p></li>
</ul>
</div></blockquote>
<p class="attribution">—<a class="reference external" href="http://lyterate.sourceforge.net/intro.html">Ly</a> introduction</p>
</div></blockquote>
<p>However, Ly introduces its own language with</p>
<ul class="simple">
<li><p>a syntax for code chunks,</p></li>
<li><p>some special ways for entering HTML tags (“currently undocumented”).</p></li>
</ul>
<p>whereas PyLit relies on the established <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> for
documentation markup and linking.</p>
</section>
<section id="xmltangle">
<h3>XMLTangle<a class="headerlink" href="#xmltangle" title="Permalink to this headline">¶</a></h3>
<p><a class="reference external" href="http://literatexml.sourceforge.net/">XMLTangle</a> is an generic tangle  program written in Python. It differs from
PyLit mainly by its choice of XML as documenting language (making the source
document harder to read for a human being).</p>
</section>
<section id="pyreport">
<h3>pyreport<a class="headerlink" href="#pyreport" title="Permalink to this headline">¶</a></h3>
<p><a class="reference external" href="http://gael-varoquaux.info/programming/pyreport-literate-programming-in-python.html">Pyreport</a> is quite similar to PyLit in its focus on Python and the use of
reStructuredText. It is a combination of <a class="reference external" href="http://en.wikipedia.org/wiki/Documentation_generator">documentation generator</a>
(processing embedded documentation) and report tool (processing Python
output).</p>
<blockquote>
<div><p>pyreport is a program that runs a python script and captures its output,
compiling it to a pretty report in a PDF or an HTML file. It can display
the output embedded in the code that produced it and can process special
comments (literate comments) according to markup languages (rst or LaTeX)
to compile a very readable document.</p>
<p>This allows for extensive literate programming in python, for generating
reports out of calculations written in python, and for making nice
tutorials.</p>
</div></blockquote>
<p>However, it is not a tool to “write documents that contain programs”.</p>
</section>
</section>
<section id="references">
<h2>References<a class="headerlink" href="#references" title="Permalink to this headline">¶</a></h2>
<div role="list" class="citation-list">
<div class="citation" id="contemporary-literate-programming" role="doc-biblioentry">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#citation-reference-1">contemporary-literate-programming</a><span class="fn-bracket">]</span></span>
<p>Vreda Pieterse, Derrick G. Kourie, and Andrew Boake:
<cite>A case for contemporary literate programming</cite>,
in SAICSIT, Vol. 75, Pages: 2-9,
Stellenbosch, Western Cape, South Africa: 2004,
available at: <a class="reference external" href="http://portal.acm.org/citation.cfm?id=1035054">http://portal.acm.org/citation.cfm?id=1035054</a></p>
</div>
</div>
</section>
</section>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/pylit-bold-framed.png" alt="Logo"/>
            </a></p>
<h1 class="logo"><a href="index.html">PyLit</a></h1>








<h3>Navigation</h3>
<ul>
<li class="toctree-l1"><a class="reference internal" href="features.html">Features</a></li>
</ul>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="usage.html">Usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1"><a class="reference internal" href="examples/index.html">Examples</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Literate Programming</a></li>
</ul>
<ul>
<li class="toctree-l1"><a class="reference internal" href="download/index.html">Requirements</a></li>
<li class="toctree-l1"><a class="reference internal" href="download/index.html#download">Download</a></li>
<li class="toctree-l1"><a class="reference internal" href="download/index.html#installation">Installation</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="examples/conf.py.html" title="previous chapter">conf.py – Sphinx configuration for PyLit</a></li>
      <li>Next: <a href="download/index.html" title="next chapter">Requirements</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
 <p class="thanks">
  <!-- Thanks to	      -->
  <!-- for hosting this site. -->
 </p>

  </body>
</html>