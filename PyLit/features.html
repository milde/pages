


<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19b2.dev: https://docutils.sourceforge.io/" />

    <title>Features &#8212; PyLit</title>
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="_static/pylit-sphinx.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="shortcut icon" href="_static/favicon.ico"/>
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Usage" href="usage.html" />
    <link rel="prev" title="PyLit" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="features">
<h1>Features<a class="headerlink" href="#features" title="Permalink to this headline">¶</a></h1>
<p><cite>PyLit</cite> (Python-Literate) provides a plain but efficient tool for literate
programming: a bidirectional converter between</p>
<ul class="simple">
<li><p>a “text” source with embedded code blocks and</p></li>
<li><p>a “code” source with the documentation in comment blocks.</p></li>
</ul>
<p>It is written in <a class="reference external" href="http://www.python.org">Python</a> but should work with any programming language that
provides a syntax for comment blocks.</p>
<p>The following features set PyLit apart from other
<a class="reference external" href="literate-programming.html#existing-frameworks">literate programming frameworks</a></p>
<section id="dual-source">
<h2>Dual Source<a class="headerlink" href="#dual-source" title="Permalink to this headline">¶</a></h2>
<p>The program source can be either in “text” or “code” format:</p>
<ul class="simple">
<li><p>The “code” source can be debugged, compiled or executed with standard
tools.</p></li>
<li><p>The “text” source can be converted to HTML or LaTeX documents, e.g. with
the Python <a class="reference external" href="https://docutils.sourceforge.io/rst.html">Docutils</a> or <a class="reference external" href="https://www.sphinx-doc.org/">Sphinx</a>.</p></li>
</ul>
<p>The <code class="docutils literal notranslate"><span class="pre">pylit</span></code> module script converts the source between “text” and “code”
formats.</p>
<p>Both formats hold the full information. Round-trips are possible without
loss.  <a class="footnote-reference brackets" href="#footnote-1" id="footnote-reference-1" role="doc-noteref"><span class="fn-bracket">[</span>1<span class="fn-bracket">]</span></a></p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="footnote-1" role="note">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-1">1</a><span class="fn-bracket">]</span></span>
<p>Given the <code class="docutils literal notranslate"><span class="pre">--strip</span></code> option, PyLit will “export” to a format by
stripping code or text parts during the conversion.</p>
</aside>
</aside>
<section id="advantages">
<h3>Advantages<a class="headerlink" href="#advantages" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>Work on code or documentation parts can be done in adapted editor modes or
environments providing formatting tools, syntax highlight, outlining, and
other conveniences.</p></li>
<li><p>Debugging is straightforward (using the “code” format). Fixes are easily
passed to the “text” format (just calling <code class="docutils literal notranslate"><span class="pre">pylit.py</span></code> on the “code”
source).</p></li>
</ul>
</section>
<section id="limitations">
<h3>Limitations<a class="headerlink" href="#limitations" title="Permalink to this headline">¶</a></h3>
<ul>
<li><p>The parallel existence of code and text formats might lead to confusion on
which is the up to date, authoritative source.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">pylit</span></code> module has some provisions for <a class="reference external" href="examples/pylit.py.html#dual-source-handling">dual source handling</a>.</p>
</li>
<li><p>While by default, an output file is not overwritten if it is newer than
the input file, loss of changes can occur when both formats are edited in
parallel.</p></li>
<li><p>PyLit does not allow the specification of a separate output file for
individual code blocks like e.g. <a class="reference external" href="https://www.cs.tufts.edu/~nr/noweb/">noweb</a>. The “dual source” concept limits
the choice to one output file per input file. However, this can be
compensated by the use of the <a class="reference external" href="https://docutils.sourceforge.io/docs/ref/rst/directives.html#including-an-external-document-fragment">include directive</a>.
See the <a class="reference external" href="tutorial/index.html">tutorial</a> for an example.</p></li>
<li><p>Only one comment string (including whitespace) can be specified as
“text block marker”.</p>
<p>It would be possible to specify a list of matching comment strings (or
a regular expression), however as the comment string is not stored in
the text source, all text blocks will get one common comment string if
(re)converted to code source – possibly loosing vital information.</p>
</li>
</ul>
</section>
</section>
<section id="simplicity">
<h2>Simplicity<a class="headerlink" href="#simplicity" title="Permalink to this headline">¶</a></h2>
<p>Pylit is a <a class="reference external" href="http://python.org/doc/2.2.3/dist/simple-example.html#SECTION000220000000000000000">pure Python module</a>, i.e.  a module written in Python and
contained in a single file (<code class="docutils literal notranslate"><span class="pre">pylit.py</span></code>).</p>
<p>PyLit converts between a “text document with embedded code” and “commented
code” but does not provide code re-ordering or multiple expansion of
definitions. Some call this <a class="reference external" href="literate-programming/index.html#semi-literate">semi-literate programming</a>.</p>
<p>Doing without “named chunks” reduces the complexity of the program and
enables a line-by-line correspondence between text source and code source.</p>
</section>
<section id="markup-with-restructuredtext">
<h2>Markup with reStructuredText<a class="headerlink" href="#markup-with-restructuredtext" title="Permalink to this headline">¶</a></h2>
<p>PyLit uses <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> from the Python <a class="reference external" href="https://docutils.sourceforge.io/rst.html">Docutils</a> for documentation
text markup. <a class="footnote-reference brackets" href="#footnote-2" id="footnote-reference-2" role="doc-noteref"><span class="fn-bracket">[</span>2<span class="fn-bracket">]</span></a></p>
<ul>
<li><p>reStructuredText is an “ASCII-markup” language. Its syntax is very similar
to common conventions used to structure “ASCII text” like e.g. emails.
Thus it is more easy to read and to edit in a text editor than e.g. TeX,
HTML, or XML.</p></li>
<li><p>reStructuredText is well documented, and actively maintained.</p></li>
<li><p>Special editor modes for editing reStructuredText already exist (see
<a class="reference external" href="https://docutils.sourceforge.io/tools/editors/README.html">Editor Support for reStructuredText</a>.)</p></li>
<li><p>With <a class="reference external" href="https://docutils.sourceforge.io/rst.html">Docutils</a>, the text source can be converted to a nice looking,
hyper-linked, browsable HTML documentation as well as to a LaTeX document
(and with pdflatex further to a printer-ready PDF manual).</p>
<p>The Python Documentation Generator <a class="reference external" href="https://www.sphinx-doc.org/">Sphinx</a>, adds support for more complex
projects including extensive cross-references, automatic indices and
automatic highlighting of source code.</p>
</li>
</ul>
<p>Most of the time, a programmer will see the source in a “text editor”. With
reStructuredText as documenting language, not only the pretty printed
documentation, but also the source (be it in “text” or “code” format) is a
<em>“combination of documentation and source together in a fashion suited for
reading by human beings”</em>, i.e. a <em>literate program</em> according to the
“inclusive” definition in the <a class="reference external" href="http://www.literateprogramming.com/lpfaq.pdf">Literate Programming FAQ</a>.</p>
<p>The <a class="reference external" href="tutorial/index.html">tutorial</a> and the <a class="reference external" href="examples/index.html">examples</a> show what code and text source may look like.</p>
<section id="syntax-highlight">
<h3>Syntax Highlight<a class="headerlink" href="#syntax-highlight" title="Permalink to this headline">¶</a></h3>
<p>While not available in standard Docutils, syntax highlight is an add-on in
many reStructuredText based applications like the <a class="reference external" href="https://www.sphinx-doc.org/">Sphinx</a> Python
Documentation Generator.</p>
<p>There is documentation and experimental code in the Docutils <a class="reference external" href="https://docutils.sourceforge.io/sandbox/">Sandbox</a>
project <a class="reference external" href="https://docutils.sourceforge.io/sandbox/code-block-directive">code-block directive</a> to implement
<a class="reference external" href="https://docutils.sourceforge.io/sandbox/code-block-directive/docs/syntax-highlight.html">syntax highlight in core Docutils</a>.</p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="footnote-2" role="note">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-2">2</a><span class="fn-bracket">]</span></span>
<p>Actually, only the syntax for literal blocks is needed for PyLit to
convert from text to code. If no pretty-printed or web-ready document
is desired, the programmer does not need to know anything else about
reStructuredText.</p>
</aside>
</aside>
</section>
</section>
<section id="python-doctest-support">
<h2>Python Doctest Support<a class="headerlink" href="#python-doctest-support" title="Permalink to this headline">¶</a></h2>
<p>Pylit supports <a class="reference external" href="http://docs.python.org/library/doctest.html">Python doctests</a> in documentation blocks. For details see
the <a class="reference external" href="tutorial/index.html#doctests">tutorial section</a> and the <a class="reference external" href="examples/literate-doctests.html">literate doctests example</a>.</p>
</section>
</section>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/pylit-bold-framed.png" alt="Logo"/>
            </a></p>
<h1 class="logo"><a href="index.html">PyLit</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Features</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#dual-source">Dual Source</a></li>
<li class="toctree-l2"><a class="reference internal" href="#simplicity">Simplicity</a></li>
<li class="toctree-l2"><a class="reference internal" href="#markup-with-restructuredtext">Markup with reStructuredText</a></li>
<li class="toctree-l2"><a class="reference internal" href="#python-doctest-support">Python Doctest Support</a></li>
</ul>
</li>
</ul>
<ul>
<li class="toctree-l1"><a class="reference internal" href="usage.html">Usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1"><a class="reference internal" href="examples/index.html">Examples</a></li>
<li class="toctree-l1"><a class="reference internal" href="literate-programming.html">Literate Programming</a></li>
</ul>
<ul>
<li class="toctree-l1"><a class="reference internal" href="download/index.html">Requirements</a></li>
<li class="toctree-l1"><a class="reference internal" href="download/index.html#download">Download</a></li>
<li class="toctree-l1"><a class="reference internal" href="download/index.html#installation">Installation</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">PyLit</a></li>
      <li>Next: <a href="usage.html" title="next chapter">Usage</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
 <p class="thanks">
  <!-- Thanks to	      -->
  <!-- for hosting this site. -->
 </p>

  </body>
</html>